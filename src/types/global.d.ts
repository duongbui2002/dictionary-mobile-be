declare module "mongoose" {
  interface QueryOptions {
    select?: string;
    populate?: PopulateOptions | (PopulateOptions | string)[];
    nullable?: boolean;
  }

  interface SaveOptions {
    select?: string;
    populate?: PopulateOptions | (PopulateOptions | string)[];
    nullable?: boolean;
  }

  interface SaveOption {
    populate?: PopulateOptions | (PopulateOptions | string)[];
  }
}

declare module "mongoose" {
  interface PaginateOptions {
    select?: string;
    populate?: PopulateOptions | (PopulateOptions | string)[];
    nullable?: boolean;
    aggregateOptions?: {
      totalDoc: number
    }
  }

  interface AggregatePaginateResult<T> {
    docs: T[];
    totalDocs: number;
    limit: number;
    page?: number | undefined;
    totalPages: number;
    nextPage?: number | null | undefined;
    prevPage?: number | null | undefined;
    pagingCounter: number;
    hasPrevPage: boolean;
    hasNextPage: boolean;
    meta?: any;
    [customLabel: string]: T[] | number | boolean | null | undefined;
  }

  interface AggregatePaginateModel<T extends Document> extends import("mongoose").Model<T> {
    aggregatePaginate(
      query?: T[],
      options?: import("mongoose").PaginateOptions,
      callback?: (err: any, result: AggregatePaginateResult<T>) => void,
    ): Promise<AggregatePaginateResult<T>>;
  }
}