import { MONGO_URI } from "@configs/env";
import { AppController } from "@src/app.controller";
import { Module } from "@nestjs/common";
import { APP_FILTER, RouterModule } from "@nestjs/core";
import { MongooseModule } from "@nestjs/mongoose";
import { AccountsModule } from "@modules/accounts/accounts.module";
import { DatabaseModule } from "@common/database/database.module";
import { AllExceptionsFilter } from "@common/filters/all-exceptions.filter";
import { AuthModule } from "@modules/auth/auth.module";
import { TokensModule } from "@modules/tokens/tokens.module";
import { EmailsModule } from "@modules/emails/emails.module";
import { WordsModule } from './modules/words/words.module';
import { BookmarksModule } from './modules/bookmarks/bookmarks.module';
import { ExamsModule } from './modules/exams/exams.module';
import { QuestionsModule } from './modules/questions/questions.module';
import { QuestionDetailsModule } from './modules/question-details/question-details.module';
import { UserExamsModule } from './modules/user-exams/user-exams.module';
import { SearchModuleModule } from './modules/search-module/search-module.module';

@Module({
  imports: [
    RouterModule.register([{
      path: "api",
      children: [
        AccountsModule,
        AuthModule,
        EmailsModule
      ]
    }]),
    MongooseModule.forRoot(MONGO_URI),
    DatabaseModule,
    AccountsModule,
    AuthModule,
    TokensModule,
    EmailsModule,
    WordsModule,
    BookmarksModule,
    ExamsModule,
    QuestionsModule,
    QuestionDetailsModule,
    UserExamsModule,
    SearchModuleModule
  ],
  controllers: [
    AppController
  ],
  providers: [{
    provide: APP_FILTER,
    useClass: AllExceptionsFilter
  }]
})
export class AppModule {
}
