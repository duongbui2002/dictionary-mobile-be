import { PopulateOptions } from "mongoose";

export const autoPopulate = (authorPath = null, additionalPopulateQuery?: PopulateOptions | (PopulateOptions | string)[]) => function(next) {
  let populateQuery: PopulateOptions | (PopulateOptions | string)[] = [{
    path: authorPath || "author",
    model: "Account",
    select: "username displayName avatar"
  }];

  (additionalPopulateQuery) ?  !Array.isArray(additionalPopulateQuery) ? populateQuery.push(additionalPopulateQuery) : populateQuery.push(...additionalPopulateQuery) : populateQuery;

  this.populate(populateQuery);
  next();
};
