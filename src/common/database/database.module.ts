import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AccountSchema } from '@modules/accounts/account.schema';
import { DefaultAccount } from '@src/utils/default-account';
import { TokenSchema } from '@modules/tokens/token.schema';
import { EmailSchema } from '@modules/emails/schema/emails.schema';
import { WordSchema } from '@modules/words/word.schema';
import { WordDetailSchema } from '@modules/words/sub-modules/word-details/word-details.schema';
import { BookMarkSchema } from '@modules/bookmarks/bookmark.schema';
import { ExamSchema } from '@modules/exams/exam.schema';
import {QuestionSchema} from "@modules/questions/question.schema";
import {QuestionDetailSchema} from "@modules/question-details/question-detail.schema";
import {UserExamSchema} from "@modules/user-exams/user-exam.schema";

const mongooseModulesForFeature = [
  {
    name: 'Account',
    schema: AccountSchema,
  },
  {
    name: 'Token',
    schema: TokenSchema,
  },
  {
    name: 'Email',
    schema: EmailSchema,
  },
  {
    name: 'Word',
    schema: WordSchema,
  },
  {
    name: 'WordDetail',
    schema: WordDetailSchema,
  },
  {
    name: 'BookMark',
    schema: BookMarkSchema,
  },
  {
    name: 'Exam',
    schema: ExamSchema,
  },
  {
    name: 'Question',
    schema: QuestionSchema,
  },
  {
    name: 'QuestionDetail',
    schema: QuestionDetailSchema,
  },
  {
    name: 'UserExam',
    schema: UserExamSchema,
  },
];

@Global()
@Module({
  imports: [
    MongooseModule.forFeature(mongooseModulesForFeature),
    MongooseModule.forFeatureAsync([
      {
        name: 'Account',
        useFactory: () => {
          const schema = AccountSchema;
          const accountModel = new DefaultAccount(schema);
          accountModel.generate().then((_) => {});
          return schema;
        },
      },
    ]),
  ],

  exports: [MongooseModule.forFeature(mongooseModulesForFeature)],
})
export class DatabaseModule {}
