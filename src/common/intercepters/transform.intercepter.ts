import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from "@nestjs/common";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

export interface Response<T> {
  message?: string;
  data?: any;
  success?: boolean;
  paginationOptions?: Partial<import ("mongoose").PaginateResult<T>>;
}

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {
    return next.handle().pipe(map(res => {
      if (res) return (res.hasOwnProperty("paginationOptions")) ? {
        data: res.data,
        message: res.message,
        paginationOptions: res.paginationOptions,
        success: true
      } : (res.hasOwnProperty("message")) ? {
        data: res.data,
        message: res.message,
        success: true
      } : {
        data: res,
        success: true
      };
    }));
  }
}
