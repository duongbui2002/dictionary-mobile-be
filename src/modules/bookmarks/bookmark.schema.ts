import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as paginate from 'mongoose-paginate-v2';
import mongoose from 'mongoose';
import {Word, WordDocument} from '@modules/words/word.schema';
import {Account, AccountDocument} from '@modules/accounts/account.schema';

export type BookMarkDocument = BookMark & Document;

@Schema({
  timestamps: true,
})
export class BookMark {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Account',
  })
  account: AccountDocument;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Word',
  })
  word: WordDocument;

  @Prop({
    type: Date,
  })
  createdDate: string;

  @Prop({
    default: true,
  })
  isActivated: boolean;
}

export const BookMarkSchema = SchemaFactory.createForClass(BookMark);

BookMarkSchema.plugin(paginate);
