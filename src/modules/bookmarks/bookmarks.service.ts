import { Injectable } from '@nestjs/common';
import { BaseService } from '@common/services/base.service';
import { BookMark, BookMarkDocument } from '@modules/bookmarks/bookmark.schema';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { ObjectId, PaginateModel } from 'mongoose';
import { PaginationParamsDto } from '@common/dto/pagination-params.dto';
import { Account, AccountDocument } from '@modules/accounts/account.schema';
import { WordsService } from '@modules/words/words.service';
import { Word } from '@modules/words/word.schema';

@Injectable()
export class BookmarksService extends BaseService<BookMarkDocument> {
  constructor(
    @InjectModel('BookMark')
    private bookmarkModel: PaginateModel<BookMarkDocument>,
    @InjectModel('Word')
    private readonly wordService: WordsService,
  ) {
    super(bookmarkModel);
  }

  async findAllFavoriteWordByAccountId(
    account: Account,
    pagination: PaginationParamsDto,
  ): Promise<BookMark[]> {
    return await this.bookmarkModel.find({
      account: account,
      isActivated: true
    }).populate('word');
  }

  async deleteBookmarkById(
      bookmarkId: string
  ) {
    const objId = new mongoose.Types.ObjectId(bookmarkId);
    return await this.bookmarkModel.findOneAndUpdate(objId, {
      isActivated: false
    });
  }

  async insertNewWord(account: Account, wordId: string) {
    const objId = new mongoose.Types.ObjectId(wordId);
    const employee = new this.bookmarkModel({
      account: account,
      word: objId,
    });
    return await employee.save();
  }
}
