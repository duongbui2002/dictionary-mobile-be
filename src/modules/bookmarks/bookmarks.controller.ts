import {Controller, Delete, Get, Param, Post, Query, UseGuards} from '@nestjs/common';
import { BookmarksService } from '@modules/bookmarks/bookmarks.service';
import { Account } from '@modules/accounts/account.schema';
import { AccountDecorator } from '@common/decorators/account.decorator';
import { PaginationParamsDto } from '@common/dto/pagination-params.dto';
import { AuthGuard } from '@common/guards/auth.guard';

@Controller('bookmarks')
export class BookmarksController {
  constructor(private bookmarkService: BookmarksService) {}

  @Get()
  @UseGuards(AuthGuard)
  findById(
    @AccountDecorator() account: Account,
    @Query() pagination: PaginationParamsDto,
  ) {
    return this.bookmarkService.findAllFavoriteWordByAccountId(
      account,
      pagination,
    );
  }
  @Post('/new/:wordId')
  @UseGuards(AuthGuard)
  createNewBookmark(
    @AccountDecorator() account: Account,
    @Param('wordId') wordId: string,
  ) {
    return this.bookmarkService.insertNewWord(account, wordId);
  }

  @Delete(':bookmarkId')
  @UseGuards(AuthGuard)
  deleteBookmark(
      @AccountDecorator() account: Account,
      @Param('bookmarkId') bookmarkId: string,
  ) {
    return {
      success: true,
      data: this.bookmarkService.deleteBookmarkById(bookmarkId)
    };
  }
}
