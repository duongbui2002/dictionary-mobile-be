import { Module } from '@nestjs/common';
import { BookmarksService } from './bookmarks.service';
import { BookmarksController } from './bookmarks.controller';
import {WordsModule} from "@modules/words/words.module";

@Module({
  providers: [BookmarksService],
  controllers: [BookmarksController],
  imports: [WordsModule],
})
export class BookmarksModule {}
