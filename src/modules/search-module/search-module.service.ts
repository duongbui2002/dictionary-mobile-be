import {Injectable} from '@nestjs/common';
import {ElasticsearchService} from "@nestjs/elasticsearch";
import {PaginatedDocumentsResponseDto, PaginationParamsDto} from "@common/dto/pagination-params.dto";

@Injectable()
export class SearchModuleService {
    vocabulariesIndex = "dictionaries"

    constructor(private elasticSearchService: ElasticsearchService) {
    }

    async searchWords(text: string, pagination: PaginationParamsDto) {
        let results = await this.elasticSearchService.search({
            index: this.vocabulariesIndex,
            from: (pagination.page - 1) * pagination.limit,
            size: pagination.limit,
            query: {
                match_phrase_prefix: {
                    "name": text
                }
            }
        })
        const hits = results.hits.hits
        let resData = hits.map((item) => item._source)
        let res = new PaginatedDocumentsResponseDto()
        res.data = resData;
        res.paginationOptions = {
            page:   pagination.page,
            // @ts-ignore
            totalDocs: results.hits.total
        }
        return res;
    }

    // async findWordById(id:string){
    //     let results = await this.elasticSearchService.search({
    //         index: this.vocabulariesIndex,
    //         "bool":{
    //
    //         }
    //     })
    // }

}
