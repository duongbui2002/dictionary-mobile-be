import {Global, Module} from '@nestjs/common';
import { SearchModuleService } from './search-module.service';
import {ElasticsearchModule} from "@nestjs/elasticsearch";

@Global()
@Module({
  providers: [SearchModuleService],
  imports: [ElasticsearchModule.register({
    cloud:{
      id:"c2b543bad6e64eb987cd4bd6b37fba75:dXMtY2VudHJhbDEuZ2NwLmNsb3VkLmVzLmlvJDQ4N2Q3OWI5YzI2ODQ4ZTQ4YmUzYWQ1OTU5Y2JkMTIwJGJmODBkNjYzNzFiZDQ3MWQ5M2FmYzUxMjJmOTU4YTU2"
    },
    auth: {
      username: "elastic",
      password: "G9f0dFa1jlFv1GpQAYfT8OQl"
    }
  })],
  exports: [
      SearchModuleService
  ]
})
export class SearchModuleModule {}
