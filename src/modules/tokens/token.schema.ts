import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as paginate from "mongoose-paginate-v2";
import * as mongoose from "mongoose";
import { Account } from "@modules/accounts/account.schema";
import TokenEnum from "@modules/tokens/token.enum";

export type TokenDocument = Token & Document;

@Schema({
  timestamps: true
})
export class Token {
  @Prop({
    required: true,
    index: true,
    trim: true
  })
  token: string;

  @Prop({
    required: true,
    type: mongoose.Schema.Types.ObjectId, ref: "Account"
  })
  account: Account;

  @Prop({
    enum: TokenEnum
  })
  type: TokenEnum;

  @Prop({
    required: true
  })
  expiresAt: Date;
  @Prop({
    type: Boolean,
    default: false
  })
  isBlackListed?: boolean;
}

export const TokenSchema = SchemaFactory.createForClass(Token);

TokenSchema.index({ expiresAt: 1 }, { expireAfterSeconds: 0 });

TokenSchema.plugin(paginate);
