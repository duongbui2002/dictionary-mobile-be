enum TokenEnum {
  REFRESH = 'refresh',
  ACCESS = 'access',
  EMAIL_VERIFY = 'email_verify',
  RESET_PASS = 'reset_pass',
}

export const TokenTypes = Object.values(TokenEnum);

export type TokenType = typeof TokenTypes[number];

export default TokenEnum;
