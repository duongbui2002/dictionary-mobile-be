import { HttpException, HttpStatus, Injectable, Logger } from "@nestjs/common";
import { TokenDocument } from "@modules/tokens/token.schema";
import { InjectModel } from "@nestjs/mongoose";
import { FilterQuery, PaginateModel, QueryOptions } from "mongoose";
import TokenEnum, { TokenTypes } from "@modules/tokens/token.enum";
import {
  ACCESS_TOKEN_EXPIRES_TIME,
  ACCESS_TOKEN_SECRET,
  EMAIL_TOKEN_SECRET, EMAIL_VERIFY_TOKEN_EXPIRES_TIME, REFRESH_TOKEN_EXPIRES_TIME,
  REFRESH_TOKEN_SECRET, RESET_PASS_TOKEN_EXPIRES_TIME,
  RESET_PASS_TOKEN_SECRET
} from "@configs/env";
import * as moment from "moment";
import { AuthTokenPayload, TokenPayload } from "@modules/auth/dto/auth-config.dto";
import * as jwt from "jsonwebtoken";
import { AccountDocument } from "@modules/accounts/account.schema";
import { VerifyTokenFromEmailDto } from "@modules/auth/dto/verify-token-from-email.dto";

@Injectable()
export class TokensService {

  constructor(@InjectModel("Token") private tokenModel: PaginateModel<TokenDocument>) {
  }

  async create(token: Partial<TokenDocument>) {
    return await this.tokenModel.create(token);
  }

  async findOne(filter: FilterQuery<TokenDocument>, options?: QueryOptions) {
    return this.tokenModel.findOne(filter, options);
  }

  async deleteOne(filter: FilterQuery<TokenDocument>, options?: QueryOptions) {
    return this.tokenModel.findOneAndDelete(filter, options);
  }

  async count(filter: FilterQuery<TokenDocument>) {
    return this.tokenModel.countDocuments(filter);
  }

  async generateAuthTokens(payload: AuthTokenPayload, account: AccountDocument) {

    let {
      accessTokenExpiresIn, refreshTokenExpiresIn
    } = await this.generateTokensExpirationTime();

    const access_token = this.generateToken({
      ...payload,
      type: TokenEnum.ACCESS
    }, TokenEnum.ACCESS, { expiresIn: accessTokenExpiresIn });

    let refresh_token;
    if (!payload.teamId) {
      refresh_token = this.generateToken({
        ...payload,
        type: TokenEnum.REFRESH
      }, TokenEnum.REFRESH, { expiresIn: refreshTokenExpiresIn });

      await this.saveToken(
        refresh_token,
        account,
        TokenEnum.REFRESH,
        refreshTokenExpiresIn
      );
    }

    return {
      access_token,
      refresh_token,
      accessTokenExpiresIn,
      refreshTokenExpiresIn
    };
  }

  async saveToken(token: string, account: AccountDocument, type: TokenEnum, expiresIn?: number) {
    await this.create({
      token,
      account: account._id,
      expiresAt: moment().utcOffset("+0700").add(expiresIn, "s").toDate(),
      type: type
    });
  }

  generateToken(payload: TokenPayload, type: TokenEnum, options?: jwt.SignOptions) {
    const tokenSecret = this.getTokenSecret(type);
    // const privateKey = Buffer.from(privateKeyBase64, "base64").toString("ascii");
    return jwt.sign(payload, tokenSecret, {
      algorithm: "HS256",
      ...options
    });
  }

  getTokenSecret(tokenType: TokenEnum) {
    switch (tokenType) {
      case TokenEnum.ACCESS:
        return ACCESS_TOKEN_SECRET;

      case TokenEnum.REFRESH:
        return REFRESH_TOKEN_SECRET;

      case TokenEnum.EMAIL_VERIFY:
        return EMAIL_TOKEN_SECRET;

      case TokenEnum.RESET_PASS:
        return RESET_PASS_TOKEN_SECRET;

    }
  }

  generateTokensExpirationTime(type?: TokenEnum) {
    const expiresTime = {};

    for (const type of TokenTypes) {
      let tokenExpiresTimeByType;
      switch (type) {
        case TokenEnum.ACCESS:
          tokenExpiresTimeByType = ACCESS_TOKEN_EXPIRES_TIME;
          break;

        case TokenEnum.REFRESH:
          tokenExpiresTimeByType = REFRESH_TOKEN_EXPIRES_TIME;
          break;

        case TokenEnum.EMAIL_VERIFY:
          tokenExpiresTimeByType = EMAIL_VERIFY_TOKEN_EXPIRES_TIME;
          break;

        case TokenEnum.RESET_PASS:
          tokenExpiresTimeByType = RESET_PASS_TOKEN_EXPIRES_TIME;
          break;
      }

      if (tokenExpiresTimeByType) {
        expiresTime[type] = moment.duration({
          [tokenExpiresTimeByType.replace(/[^A-Za-z]/g, "")]: parseInt(tokenExpiresTimeByType.match(/\d+/)[0])
        }).asSeconds();
      }
    }

    return (type) ? expiresTime[type] : {
      accessTokenExpiresIn: expiresTime[TokenEnum.ACCESS],
      refreshTokenExpiresIn: expiresTime[TokenEnum.REFRESH],
      emailVerifyTokenExpiresIn: expiresTime[TokenEnum.EMAIL_VERIFY],
      changePasswordTokenExpiresIn: expiresTime[TokenEnum.RESET_PASS]
    };
  }

  verifyToken(token: string, type: TokenEnum): any {
    try {
      const tokenSecret = this.getTokenSecret(type);

      return jwt.verify(token, tokenSecret);
    } catch (e) {
      this.deleteOne({ token }).then(_ => {
      });
      throw e;
    }
  }

}
