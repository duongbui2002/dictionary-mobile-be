import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { PaginateModel } from 'mongoose';
import { Exam, ExamDocument } from '@modules/exams/exam.schema';
import { BaseService } from '@common/services/base.service';
import { CreateExamDto } from '@modules/exams/dto/create-exam.dto';
import { QuestionsService } from '@modules/questions/questions.service';
import { QuestionDetailsService } from '@modules/question-details/question-details.service';
import { QuestionDocument } from '@modules/questions/question.schema';
import { QuestionDetail } from '@modules/question-details/question-detail.schema';
import { CreateQuestionDto } from '@modules/questions/dto/create-question.dto';

@Injectable()
export class ExamsService extends BaseService<ExamDocument> {
  constructor(
    @InjectModel('Exam') private examModel: PaginateModel<ExamDocument>,
    private readonly questionService: QuestionsService,
    private readonly questionDetailService: QuestionDetailsService,
  ) {
    super(examModel);
  }
  async createExam(dto: CreateExamDto): Promise<any> {
    const exam = new this.examModel({
      name: dto.name,
      topic: dto.topic,
    });
    await exam.save();
    dto.questions.map(async (item) => {
      const question = await this.questionService.createQuestion(item, exam);
      item.questionDetails.map((questionD) => {
        const questionDetail = this.questionDetailService.createQuestionDetail(
          questionD,
          question,
        );
      });
    });
  }

  async createQuestion(dto: CreateQuestionDto, examId: string): Promise<any> {
    const exam = await this.examModel.findById(examId);
    const question = await this.questionService.createQuestion(dto, exam);
    dto.questionDetails.map((questionD) => {
      const questionDetail = this.questionDetailService.createQuestionDetail(
        questionD,
        question,
      );
    });
  }
  async getExamByid(examId: string): Promise<Exam> {
    const objId = new mongoose.Types.ObjectId(examId);
    const exam = await this.examModel.findById(objId).populate('questions');
    // exam.ques = exam.questions;
    // console.log(exam);
    // console.log(exam.questions);
    // console.log(exam)
    const questions = await this.questionService.findByExamId(exam);
    for (let i = 0; i < questions.length; i++) {
      const questionDetail = await this.questionDetailService.findByQuestionId(
        questions[i],
      );
      // console.log(questionDetail);
      questions[i].answers = questionDetail;
    }
    // questions.map(async (question) => {
    //   const questionDetail = await this.questionDetailService.findByQuestionId(
    //     question,
    //   );
    //   console.log(questionDetail);
    //   question.answers = questionDetail;
    // });
    // console.log(questions);
    exam.ques = questions;
    return exam;
  }

  async getAllExams(): Promise<Exam[]> {
    const exam = await this.examModel.find();
    return exam;
  }
}
