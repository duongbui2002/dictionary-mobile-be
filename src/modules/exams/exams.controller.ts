import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Post, Query,
  UseGuards,
} from '@nestjs/common';
import { ExamsService } from '@modules/exams/exams.service';
import { AuthGuard } from '@common/guards/auth.guard';
import { CreateExamDto } from '@modules/exams/dto/create-exam.dto';
import { AccountDecorator } from '@common/decorators/account.decorator';
import { Account } from '@modules/accounts/account.schema';
import {CreateQuestionDto} from "@modules/questions/dto/create-question.dto";

@Controller('exams')
export class ExamsController {
  constructor(private examService: ExamsService) {}
  @Post()
  @HttpCode(201)
  @UseGuards(AuthGuard)
  async createOrder(@Body() dto: CreateExamDto): Promise<any> {
    return this.examService.createExam(dto);
  }

  @Post(':examId')
  @HttpCode(201)
  async createQuestion(@Body() dto: CreateQuestionDto, @Param('examId') examId: string): Promise<any> {
    return this.examService.createQuestion(dto, examId);
  }

  @Get(':examId')
  @UseGuards(AuthGuard)
  getExam(@Param('examId') examId: string) {
    return this.examService.getExamByid(examId);
  }
  @Get('')
  @UseGuards(AuthGuard)
  getAllExam() {
    return this.examService.getAllExams();
  }
}
