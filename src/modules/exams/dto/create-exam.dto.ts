import {
  IsArray,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { CreateQuestionDto } from '@modules/questions/dto/create-question.dto';

export class CreateExamDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  topic: string;

  @IsArray()
  questions: CreateQuestionDto[];
}
