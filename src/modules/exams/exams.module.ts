import { Module } from '@nestjs/common';
import { ExamsService } from './exams.service';
import { ExamsController } from './exams.controller';
import { QuestionsModule } from '@modules/questions/questions.module';
import { QuestionDetailsModule } from '@modules/question-details/question-details.module';

@Module({
  providers: [ExamsService],
  controllers: [ExamsController],
  imports: [QuestionsModule, QuestionDetailsModule],
  exports: [ExamsService]
})
export class ExamsModule {}
