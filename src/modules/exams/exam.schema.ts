import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as paginate from 'mongoose-paginate-v2';
import {Exclude, Type} from 'class-transformer';
import { BookMark } from '@modules/bookmarks/bookmark.schema';
import { Question } from '@modules/questions/question.schema';

export type ExamDocument = Exam & Document;

@Schema({
  timestamps: true,
})
export class Exam {
  @Prop({
    type: String,
    required: true,
  })
  name: string;

  @Prop({
    type: String,
  })
  topic: string;

  @Prop({
    default: true,
  })
  isActivated: boolean;

  @Type(() => Question)
  questions: Question[];

  @Prop({
    trim: true,
  })
  ques: Question[];
}

export const ExamSchema = SchemaFactory.createForClass(Exam);

ExamSchema.plugin(paginate);
ExamSchema.virtual('questions', {
  ref: 'Question',
  localField: '_id',
  foreignField: 'exam',
});
