import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { Document } from "mongoose";
import { hashSync } from "bcryptjs";
import * as paginate from "mongoose-paginate-v2";
import { BCRYPT_SALT } from "@configs/env";
import { RoleEnum } from "@modules/auth/auth.enum";
import {WordSchema} from "@modules/words/word.schema";
import {Type} from "class-transformer";
import {WordDetail} from "@modules/words/sub-modules/word-details/word-details.schema";
import {BookMark} from "@modules/bookmarks/bookmark.schema";

export type AccountDocument = Account & Document;

@Schema({
  timestamps: true
})
export class Account {
  @Prop({
    required: true,
    trim: true,
    lowercase: true,
    unique: true
  })
  email: string;

  @Prop({
    required: true,
    trim: true,
    unique: true
  })
  username: string;

  @Prop({
    trim: true
  })
  displayName: string;

  @Prop({
    default: false
  })
  isActivated: boolean;

  @Prop({
    enum: RoleEnum,
    type: [{ type: mongoose.Schema.Types.String }],
    default: [RoleEnum.USER]
  })
  roles: string[];

  @Prop({
    type: String,
    trim: true,
    default: "https://i.imgur.com/Uoeie1w.jpg"
  })
  avatar: string;

  @Prop({
    type: String,
    trim: true,
    index: true
  })
  googleId: string;

  @Prop({
    type: String,
    trim: true,
    index: true
  })
  facebookId: string;

  @Prop({
    trim: true,
    minlength: 8,
    maxlength: 32,
    select: false
  })
  password: string;

  // additional personal info

  @Prop({
    type: String,
    trim: true
  })
  address?: string;

  @Prop({
    type: String,
    trim: true
  })
  gender?: string;

  @Prop({
    type: Date,
    trim: true
  })
  dob?: Date;

  @Prop({
    type: String,
    trim: true
  })
  phone?: string;

  @Prop({
    type: String,
    trim: true
  })
  identityNumber?: string;

  @Type(() => BookMark)
  bookmarks: BookMark[]
}

export const AccountSchema = SchemaFactory.createForClass(Account);


AccountSchema.pre("save", async function(next) {
  const doc = this;
  if (!doc.displayName) doc.displayName = doc.username;
  if (doc.isModified("password") && doc.password) doc.password = hashSync(doc.password, BCRYPT_SALT);
  next();
});

AccountSchema.pre("findOneAndUpdate", async function(next) {
  const doc = this;
  if (doc["_update"] && doc["_update"]["password"]) doc["_update"]["password"] = hashSync(doc["_update"]["password"], BCRYPT_SALT);
  next();
});

AccountSchema.virtual("bookmarks", {
  ref: "BookMark",
  localField: "_id",
  foreignField: "account"
})


AccountSchema.plugin(paginate);
