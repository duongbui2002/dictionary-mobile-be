import {
  IsArray,
  IsBoolean,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateQuestionDetailDto {
  @IsOptional()
  @IsNotEmpty()
  answer: string;

  @IsBoolean()
  @IsNotEmpty()
  isCorrect: boolean;
}
