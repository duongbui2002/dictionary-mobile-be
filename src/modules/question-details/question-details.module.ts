import { Module } from '@nestjs/common';
import { QuestionDetailsService } from './question-details.service';
import { QuestionDetailsController } from './question-details.controller';

@Module({
  providers: [QuestionDetailsService],
  controllers: [QuestionDetailsController],
  exports: [QuestionDetailsService],
})
export class QuestionDetailsModule {}
