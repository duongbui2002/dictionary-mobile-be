import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { PaginateModel } from 'mongoose';
import { Question, QuestionDocument } from '@modules/questions/question.schema';
import { BaseService } from '@common/services/base.service';
import {
  QuestionDetail,
  QuestionDetailDocument,
} from '@modules/question-details/question-detail.schema';
import { CreateQuestionDto } from '@modules/questions/dto/create-question.dto';
import { CreateQuestionDetailDto } from '@modules/question-details/dto/create-question-detail.dto';
import { Exam } from '@modules/exams/exam.schema';

@Injectable()
export class QuestionDetailsService extends BaseService<QuestionDetailDocument> {
  constructor(
    @InjectModel('QuestionDetail')
    private questionDetailModel: PaginateModel<QuestionDetailDocument>,
  ) {
    super(questionDetailModel);
  }
  getModel() {
    return this.questionDetailModel;
  }
  async createQuestionDetail(
    dto: CreateQuestionDetailDto,
    question: Question,
  ): Promise<QuestionDetail> {
    const questionDetail = new this.questionDetailModel({
      answer: dto.answer,
      isCorrect: dto.isCorrect,
      question: question,
    });
    return await questionDetail.save();
  }

  async findByQuestionId(question: Question): Promise<QuestionDetail[]> {
    return this.questionDetailModel
      .find({
        question: question,
      })
      .select('-isCorrect -isActivated');
  }

  async checkAnswer(answerId: string): Promise<any> {
    const ans = await this.questionDetailModel.findById(
      new mongoose.Types.ObjectId(answerId),
    );
    return ans.isCorrect;
  }
}
