import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import * as paginate from 'mongoose-paginate-v2';
import { Type } from 'class-transformer';
import { BookMark } from '@modules/bookmarks/bookmark.schema';
import {ExamDocument} from "@modules/exams/exam.schema";
import {QuestionDocument} from "@modules/questions/question.schema";

export type QuestionDetailDocument = QuestionDetail & Document;

@Schema({
  timestamps: true,
})
export class QuestionDetail {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Question',
  })
  question: QuestionDocument;
  @Prop({
    type: String,
    required: true,
  })
  answer: string;

  @Prop({
    type: Boolean,
    default: false,
  })
  isCorrect: boolean;
  @Prop({
    default: true,
  })
  isActivated: boolean;
}

export const QuestionDetailSchema =
  SchemaFactory.createForClass(QuestionDetail);

QuestionDetailSchema.plugin(paginate);
