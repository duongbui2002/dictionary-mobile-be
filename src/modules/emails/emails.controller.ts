import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseInterceptors,
  UploadedFiles,
  Query,
  HttpException,
  HttpStatus, UseGuards
} from "@nestjs/common";
import { EmailsService } from "./emails.service";
import { Express } from "express";
import { FilesInterceptor } from "@nestjs/platform-express";
import { SendMailDto } from "./dto/send-mail.dto";
import { AuthGuard } from "@common/guards/auth.guard";
import { AccountDecorator } from "@common/decorators/account.decorator";
import { AccountDocument } from "@modules/accounts/account.schema";

@Controller("emails")
export class EmailsController {
  constructor(private readonly emailsService: EmailsService) {
  }

  @Post("send")
  @UseGuards(AuthGuard)
  @UseInterceptors(FilesInterceptor("attachments"))
  async sendEmail(
    @UploadedFiles() attachments: Array<Express.Multer.File>,
    @Body() sendMailDto: SendMailDto,
    @AccountDecorator() account: AccountDocument
  ) {
    if (attachments) {
      Object.assign(sendMailDto, {
        attachment: attachments.map((file) => ({
          data: file.buffer,
          filename: file.originalname
        }))
      });
    }
    const movedAttachments = [];

    const domain = sendMailDto.from.split("@")[1];
    await this.emailsService.sendEmail(sendMailDto, domain);

    const emails = [];
    if (typeof sendMailDto.to === "string") {
      sendMailDto.to = [sendMailDto.to];
    }
    for (const recipient of sendMailDto.to) {
      emails.push({
        creationTime: new Date().getTime(),
        sender: sendMailDto.from,
        from: sendMailDto.from,
        recipient: recipient,
        subject: sendMailDto.subject,
        strippedText: sendMailDto?.html?.replace(/(<([^>]+)>)/gi, ""),
        data: {
          from: sendMailDto.from,
          sender: sendMailDto.from,
          timestamp: new Date().getTime(),
          subject: sendMailDto.subject,
          recipient: recipient,
          domain: domain
        },
        bodyHTML: sendMailDto.html,
        owner: account._id,
        hasAttachment:
          sendMailDto.attachment && sendMailDto.attachment.length > 0,
        attachments: movedAttachments
      });
    }

    await this.emailsService.create(emails);

    return "Sent email successful";
  }

  @Get("")
  async getEmails(
    @Query("placeholder") placeholder: string, @Query() query
  ) {
    const filter = {};

    Object.assign(filter, {
      isDeleted: { $ne: true }
    });

    switch (placeholder) {
      case "inbox":
        break;
      case "sent":
        delete filter["recipient"];
        // Object.assign(filter, { sender: req.account.activeEmail });
        break;
      case "starred":
        Object.assign(filter, { isStarred: true });
        break;
      case "trash":
        Object.assign(filter, { isDeleted: true });
        break;
      default:
    }

    const data = await this.emailsService.findAll(filter);
    return data;
  }

  @Get(":id/:placeholder")
  async getEmail(@Param() param) {
    const filter = {
      _id: param.id,
      isDeleted: { $ne: true }
    };
    switch (param.placeholder) {
      case "inbox":
        break;
      case "sent":
        delete filter["recipient"];
        // Object.assign(filter, {sender: sendMailDto.activeEmail});
        break;
      case "starred":
        Object.assign(filter, { isStarred: true });
        break;
      case "trash":
        Object.assign(filter, { isDeleted: true });
        break;
      default:
        throw new HttpException("Invalid placeholder", HttpStatus.BAD_REQUEST);
    }

    const data = await this.emailsService.findOne(filter);

    if (!data.isSeen) await data.updateOne({ isSeen: true });

    return data;
  }

}
