import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as mongoose from "mongoose";
import * as paginate from "mongoose-paginate-v2";
import { AccountDocument } from "@modules/accounts/account.schema";

export type EmailDocument = Email & Document;

@Schema({
  timestamps: true
})
export class Email {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  })
  owner: AccountDocument;

  @Prop({
    values: ["outgoing", "incoming"]
  })
  emailType: string;

  @Prop({})
  creationTime: number;

  @Prop({})
  id: string;

  @Prop({ required: true })
  sender: string;

  @Prop({})
  from: string;

  @Prop({})
  recipient: string;

  @Prop({})
  subject: string;

  @Prop({
    type: Object
  })
  data: object;

  @Prop({})
  strippedText: string;

  @Prop({})
  bodyHTML: string;

  @Prop({})
  hasAttachment: boolean;

  @Prop({})
  attachments: [];

  @Prop({ default: false })
  isStarred: boolean;

  @Prop({ default: false })
  isDeleted: boolean;

  @Prop({ default: false })
  isSeen: boolean;

  @Prop({})
  starredAt: string;

  @Prop({})
  deletedAt: string;
}

export const EmailSchema = SchemaFactory.createForClass(Email);

EmailSchema.pre("save", async function(next) {
  const doc = this;
  if (doc.isModified("isStarred")) doc.starredAt = new Date().toString();
  if (doc.isModified("isDeleted")) doc.deletedAt = new Date().toString();
  next();
});

EmailSchema.plugin(paginate);
