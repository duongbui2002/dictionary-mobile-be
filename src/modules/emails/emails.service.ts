import Mailgun from "mailgun.js";
import * as FormData from "form-data";
import { MailgunMessageData } from "mailgun.js/interfaces/Messages";
import { PaginateModel } from "mongoose";
import { EmailDocument } from "@modules/emails/schema/emails.schema";
import { InjectModel } from "@nestjs/mongoose";
import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { MAILGUN_API_KEY, MAILGUN_DOMAIN } from "@configs/env";
import Client from "mailgun.js/client";
import { BaseService } from "@common/services/base.service";

@Injectable()
export class EmailsService extends BaseService<EmailDocument> {
  private mailgun: Mailgun;
  private mg: Client;
  private domain: string;
  private readonly apiKey: string;

  constructor(
    @InjectModel("Email") private emailModel: PaginateModel<EmailDocument>
  ) {
    super(emailModel);
    this.domain = MAILGUN_DOMAIN;
    this.apiKey = MAILGUN_API_KEY;
    this.mailgun = new Mailgun(FormData);
    this.mg = this.mailgun.client({ username: "api", key: this.apiKey });
  }

  async sendEmail(emailBody: MailgunMessageData, domain?: string) {
    try {
      if (domain) this.domain = domain;
      await this.mg.messages.create(this.domain, {
        ...emailBody,
        "h:Content-Type": "multipart/form-data"
      });

    } catch (e) {
      throw new HttpException("Failed to send email", HttpStatus.BAD_REQUEST);
    }
  }

}
