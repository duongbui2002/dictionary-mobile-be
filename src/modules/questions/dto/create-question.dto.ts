import { IsArray, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { CreateQuestionDetailDto } from '@modules/question-details/dto/create-question-detail.dto';
import { Exam } from '@modules/exams/exam.schema';

export class CreateQuestionDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsOptional()
  exam: Exam;

  @IsArray()
  questionDetails: CreateQuestionDetailDto[];
}
