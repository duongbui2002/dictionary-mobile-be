import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as paginate from 'mongoose-paginate-v2';
import { Type } from 'class-transformer';
import { BookMark } from '@modules/bookmarks/bookmark.schema';
import mongoose from 'mongoose';
import { WordDocument } from '@modules/words/word.schema';
import { ExamDocument } from '@modules/exams/exam.schema';
import { QuestionDetail } from '@modules/question-details/question-detail.schema';

export type QuestionDocument = Question & Document;

@Schema({
  timestamps: true,
})
export class Question {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Exam',
  })
  exam: ExamDocument;
  @Prop({
    type: String,
    required: true,
  })
  name: string;
  @Prop({
    default: true,
  })
  isActivated: boolean;

  @Type(() => QuestionDetail)
  questionDetails: QuestionDetail[];

  @Prop({
    trim: true,
  })
  answers: QuestionDetail[];
}

export const QuestionSchema = SchemaFactory.createForClass(Question);

QuestionSchema.plugin(paginate);
QuestionSchema.virtual('questionDetails', {
  ref: 'QuestionDetail',
  localField: '_id',
  foreignField: 'question',
});
