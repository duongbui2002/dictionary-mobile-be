import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { Exam, ExamDocument } from '@modules/exams/exam.schema';
import { BaseService } from '@common/services/base.service';
import { Question, QuestionDocument } from '@modules/questions/question.schema';
import { CreateQuestionDto } from '@modules/questions/dto/create-question.dto';

@Injectable()
export class QuestionsService extends BaseService<QuestionDocument> {
  constructor(
    @InjectModel('Question')
    private questionModel: PaginateModel<QuestionDocument>,
  ) {
    super(questionModel);
  }

  getModel() {
    return this.questionModel;
  }

  async createQuestion(dto: CreateQuestionDto, exam: Exam): Promise<Question> {
    const question = new this.questionModel({
      name: dto.name,
      exam: exam,
    });
    return await question.save();
  }

  async findByExamId(exam: Exam): Promise<Question[]> {
    return this.questionModel
      .find({
        exam: exam,
      })
      .select('-isActivated');
  }
}
