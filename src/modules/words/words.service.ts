import {Injectable} from '@nestjs/common';
import {BaseService} from "@common/services/base.service";
import {WordDocument} from "@modules/words/word.schema";
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from "mongoose";

@Injectable()
export class WordsService extends BaseService<WordDocument> {
    constructor(@InjectModel("Word") private wordModel: PaginateModel<WordDocument>) {
        super(wordModel);
    }

    async findAllWords(){
    }
}
