import {Module} from '@nestjs/common';
import {WordDetailsModule} from './sub-modules/word-details/word-details.module';
import {WordsService} from './words.service';
import {WordsController} from './words.controller';
import {SearchModuleModule} from "@modules/search-module/search-module.module";

@Module({
    imports: [WordDetailsModule, SearchModuleModule],
    providers: [WordsService],
    controllers: [WordsController],
})
export class WordsModule {
}
