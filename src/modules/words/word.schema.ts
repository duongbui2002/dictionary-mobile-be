import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";
import {Document} from "mongoose";
import * as paginate from "mongoose-paginate-v2";
import {WordDetail} from "@modules/words/sub-modules/word-details/word-details.schema";
import {Type} from "class-transformer";
import {BookMark} from "@modules/bookmarks/bookmark.schema";

export type WordDocument = Word & Document;

@Schema({
    timestamps: true
})
export class Word {

    @Prop({
        type: String,
        required: true
    })
    name: string

    @Prop({
        type: String
    })
    pronunciation: string

    @Type(() => WordDetail)
    wordDetails: WordDetail[]
    @Type(() => BookMark)
    bookmarks: BookMark[]
}

export const WordSchema = SchemaFactory.createForClass(Word);


WordSchema.plugin(paginate);
WordSchema.virtual("wordDetails", {
    ref: "WordDetail",
    localField: "_id",
    foreignField: "word"
})
WordSchema.virtual("bookmarks", {
    ref: "BookMark",
    localField: "_id",
    foreignField: "word"
})