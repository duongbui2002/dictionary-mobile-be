import {Controller, Get, Param, Query} from '@nestjs/common';
import {WordsService} from "@modules/words/words.service";
import {PaginationParamsDto} from "@common/dto/pagination-params.dto";
import {SearchModuleService} from "@modules/search-module/search-module.service";

@Controller('words')
export class WordsController {
    constructor(private wordService: WordsService,
                private searchModule: SearchModuleService) {
    }

    @Get()
    async getWords(@Query("word") word: string = '', @Query() pagination: PaginationParamsDto) {
        return await this.searchModule.searchWords(word, pagination);
    }

    @Get(':wordId')
    async getWordById(@Param("wordId") wordId: string) {
        const data = await this.wordService.aggregate({
            _id: wordId
        }, [
            {
                $lookup: {
                    from: 'worddetails',
                    localField: '_id',
                    foreignField: 'word',
                    as: 'wordDetails'
                }
            }
        ])
        if (data.length === 0) {
            return {
                success: false,
                data: "Chả tìm thấy từ id kia ở đâu cả?"
            }
        }
        return {
            success: true,
            data: data[0]
        };
    }
}