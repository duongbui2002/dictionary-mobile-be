import {Injectable} from '@nestjs/common';
import {BaseService} from "@common/services/base.service";
import {WordDetailDocument} from "@modules/words/sub-modules/word-details/word-details.schema";
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from "mongoose";

@Injectable()
export class WordDetailsService extends BaseService<WordDetailDocument> {
    constructor(@InjectModel("WordDetail") private wordDetailModel: PaginateModel<WordDetailDocument>) {
        super(wordDetailModel);
    }
}

