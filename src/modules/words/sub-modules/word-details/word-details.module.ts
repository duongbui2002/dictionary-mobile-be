import { Module } from '@nestjs/common';
import { WordDetailsController } from './word-details.controller';
import { WordDetailsService } from './word-details.service';

@Module({
  controllers: [WordDetailsController],
  providers: [WordDetailsService]
})
export class WordDetailsModule {}
