import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";
import {Document} from "mongoose";
import * as paginate from "mongoose-paginate-v2";
import * as mongoose from "mongoose";
import {WordDocument} from "@modules/words/word.schema";

export type WordDetailDocument = WordDetail & Document;

@Schema({
    timestamps: true
})
export class WordDetail {
    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Word"
    })
    word: WordDocument

    @Prop({})
    definition: string;

    @Prop()
    wordType: string;

    @Prop({
        type: [{type: String}]
    })
    synonym: string[]


    @Prop({
        type: [{type: String}]
    })
    antonym: string[]

    @Prop({
        type: [{type: String}]
    })
    example: string[]

}


export const WordDetailSchema = SchemaFactory.createForClass(WordDetail);


WordDetailSchema.plugin(paginate);
