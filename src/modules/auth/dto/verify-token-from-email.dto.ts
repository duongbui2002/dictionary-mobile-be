import {IsBoolean, IsNotEmpty, IsOptional, IsString} from "class-validator";

export class VerifyTokenFromEmailDto {
  @IsNotEmpty()
  @IsString()
  verifyToken: string;

  @IsOptional()
  @IsBoolean()
  isRejected?: boolean;
}
