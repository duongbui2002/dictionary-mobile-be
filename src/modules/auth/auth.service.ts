import { HttpException, HttpStatus, Injectable, Logger } from "@nestjs/common";
import * as jwt from "jsonwebtoken";
import { stringify } from "querystring";
import { AccountsService } from "@modules/accounts/accounts.service";
import { compareSync } from "bcryptjs";
import { HttpService } from "@nestjs/axios";
import { generateRandomUsername } from "@src/utils/random";
import { EmailsService } from "@modules/emails/emails.service";
import emailTemplates from "@src/assets/templates/email.template";
import { RegisterAccountDto } from "@modules/auth/dto/register-account.dto";
import * as moment from "moment/moment";
import { TokensService } from "@modules/tokens/tokens.service";
import { VerifyTokenFromEmailDto } from "@modules/auth/dto/verify-token-from-email.dto";
import { TokenDocument } from "@modules/tokens/token.schema";
import {
  EMAIL_FROM,
  EMAIL_RESET_PASSWORD_URL,
  EMAIL_VERIFY_ACCOUNT_URL,
  FACEBOOK_CLIENT_ID,
  FACEBOOK_CLIENT_SECRET,
  GOOGLE_API_BASE_URL,
  GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET,
  RECAPTCHA_SECRET
} from "@configs/env";
import * as qs from "qs";
import TokenEnum from "@modules/tokens/token.enum";
import { AccountDocument } from "@modules/accounts/account.schema";
import { AuthConfig, AuthData, AuthTokenPayload, SSOConfig, TokenPayload } from "@modules/auth/dto/auth-config.dto";

@Injectable()
export class AuthService {
  supportedServicesOAuth: string[] = ["google", "facebook"];

  constructor(
    private accountService: AccountsService,
    private httpService: HttpService,
    private tokenService: TokensService,
    private emailService: EmailsService
  ) {
  }

  async register(registerAccountDto: RegisterAccountDto) {
    if (await this.accountService.count({ $or: [{ username: registerAccountDto.username }, { email: registerAccountDto.email }] }) > 0) throw new HttpException("This account is already exists", 500);
    const account = await this.accountService.createAccountInstance(registerAccountDto);

   // await this.generateVerifyTokenAndSendEmail(TokenEnum.EMAIL_VERIFY, account, registerAccountDto.recaptcha, registerAccountDto.remoteAddress);
    account.isActivated = true;
    await account.save();
    return account;
  }

  async authenticate(loginField: string, password: string) {
    let account = await this.accountService.findOne({ $or: [{ email: loginField }, { username: loginField }] }, {
      select: "username password email isActivated"
    });

    if (!account.isActivated) {
      throw new HttpException("verifyAccount", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    const check = await this.comparePassword(password, account.password);
    if (!account || !check) {
      throw new HttpException("Failed to login", HttpStatus.UNAUTHORIZED);
    }

    account = await this.accountService.findOne({ _id: account._id });
    return account;
  }

  async verifyCaptcha(captcha: string, remoteAddress: string) {
    if (!captcha) throw new HttpException("Please provide captcha", HttpStatus.BAD_REQUEST);

    // Secret key
    // Verify URL
    const query = stringify({
      secret: RECAPTCHA_SECRET,
      response: captcha,
      remoteip: remoteAddress
    });
    const verifyURL = `https://google.com/recaptcha/api/siteverify?${query}`;

    const { data } = await this.httpService.axiosRef.get(verifyURL);

    if (data.success !== undefined && !data.success) throw new HttpException("Failed to verify captcha", HttpStatus.BAD_REQUEST);
    return data;
  }

  // async generateVerifyTokenAndSendEmail(type: TokenEnum.EMAIL_VERIFY | TokenEnum.RESET_PASS, account, recaptcha: string, remoteAddress: string) {
  //   //const recaptchaRes = await this.verifyCaptcha(recaptcha, remoteAddress);
  //
  //   if (await this.tokenService.count({ account: account._id, type }) <= 0) {
  //     const verifyTokenExpiresIn = await this.tokenService.generateTokensExpirationTime(type);
  //
  //     const tokenExpiresTime = this.tokenService.generateTokensExpirationTime(type);
  //
  //     const emailToken = this.tokenService.generateToken({
  //       accountId: account._id,
  //       type
  //     }, type, { expiresIn: tokenExpiresTime });
  //
  //     await this.tokenService.saveToken(emailToken, account, type, verifyTokenExpiresIn);
  //
  //     const subject = {
  //       [TokenEnum.EMAIL_VERIFY]: "Verify Account",
  //       [TokenEnum.RESET_PASS]: "Reset Password"
  //     };
  //
  //     const htmlTemplate = {
  //       [TokenEnum.EMAIL_VERIFY]: emailTemplates.verifyEmail("https://" + recaptchaRes.hostname + EMAIL_VERIFY_ACCOUNT_URL, emailToken),
  //       [TokenEnum.RESET_PASS]: emailTemplates.changePassword("https://" + recaptchaRes.hostname + EMAIL_RESET_PASSWORD_URL, emailToken, account.username)
  //     };
  //
  //     await this.emailService.sendEmail({
  //       subject: subject[type],
  //       from: EMAIL_FROM,
  //       to: account.email,
  //       html: htmlTemplate[type]
  //     });
  //   }
  // }

  async verifyTokenFromRequest(token: string): Promise<{ account: AccountDocument }> {
    let payload;
    try {
      payload = this.tokenService.verifyToken(token, TokenEnum.ACCESS);
    } catch (e) {
      Logger.error(e);
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }

    const account = await this.accountService.findOne({ _id: payload.accountId }, { nullable: true });

    if (!account) {
      throw new HttpException("Account does not exist", HttpStatus.UNAUTHORIZED);
    }

    return { account };

  }

  async comparePassword(password: string, storePasswordHash: string): Promise<boolean> {
    return compareSync(password, storePasswordHash);
  }


  async getTokensFromOAuth(tokenUrl: string, authConfig: AuthConfig, authData: AuthData, options: {
    isQueryStringRequired?: boolean
  } = {}) {
    let body = {};

    switch (authConfig.grantType) {
      case "authorization_code":
        body = {
          grant_type: authConfig.grantType,
          redirect_uri: authConfig.redirectUri,
          client_id: authConfig.clientID,
          client_secret: authConfig.clientSecret,
          code: authData.code
        };
        break;
      case "refresh_token":
        body = {
          grant_type: authConfig.grantType,
          client_id: authConfig.clientID,
          client_secret: authConfig.clientSecret,
          refresh_token: authData.refreshToken
        };
        break;
      default:
        throw new HttpException("Invalid grant type", HttpStatus.BAD_REQUEST);
    }

    try {
      let data;
      if (options.isQueryStringRequired && options.isQueryStringRequired === true) {
        data = (await this.httpService.axiosRef.post(tokenUrl, qs.stringify(body), {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Accept-Encoding": "gzip,deflate,compress"
          }
        })).data;

      } else {
        data = (await this.httpService.axiosRef.post(tokenUrl, body, {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept-Encoding": "gzip,deflate,compress"
          }
        })).data;
      }

      return data;
    } catch (e: any) {
      console.log(e);
      throw new HttpException("Failed to retrieve token", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async authenticateWithSSO(service: string, ssoConfig: SSOConfig) {
    if (!this.supportedServicesOAuth.includes(service)) {
      throw new HttpException("Unsupported services", HttpStatus.BAD_REQUEST);
    }
    let tokenUrl, data, accountFromSSO, account;

    switch (service) {
      case "facebook":
        tokenUrl = `https://graph.facebook.com/oauth/access_token`;
        data = await this.getTokensFromOAuth(tokenUrl, {
          clientID: FACEBOOK_CLIENT_ID,
          clientSecret: FACEBOOK_CLIENT_SECRET,
          grantType: ssoConfig.grantType,
          redirectUri: ssoConfig.redirectUri
        }, {
          code: ssoConfig.authorizationCode
        });

        accountFromSSO = (await this.httpService.axiosRef.get(`https://graph.facebook.com/me?fields=id,name,email&access_token=${data.access_token}`, {}))["data"];
        const avatar = (await this.httpService.axiosRef.get(`https://graph.facebook.com/${accountFromSSO.id}/picture?height=200&width=200&access_token=${data.access_token}&redirect=false&type=large`, {}))["data"];

        Object.assign(accountFromSSO, {
          avatar: avatar.data.url,
          fullName: accountFromSSO.name,
          type: "facebook"

        });
        break;

      case "google":
        tokenUrl = `${GOOGLE_API_BASE_URL}/oauth2/v4/token`;
        data = await this.getTokensFromOAuth(tokenUrl, {
          clientID: GOOGLE_CLIENT_ID,
          clientSecret: GOOGLE_CLIENT_SECRET,
          grantType: ssoConfig.grantType,
          redirectUri: ssoConfig.redirectUri
        }, {
          code: ssoConfig.authorizationCode
        });


        accountFromSSO = (await this.httpService.axiosRef.get(`${GOOGLE_API_BASE_URL}/userinfo/v2/me`, {
          headers: {
            Authorization: `Bearer ${data.access_token}`
          }
        }))["data"];

        Object.assign(accountFromSSO, {
          avatar: accountFromSSO.picture,
          fullName: accountFromSSO.name,
          type: "google"
        });

        break;
    }
    let randomUserName;

    account = await this.accountService.findOne({ [`${service}Id`]: accountFromSSO.id }, { nullable: true });
    if (!account) {
      account = await this.accountService.findOne({ email: accountFromSSO.email }, { nullable: true });

      if (account) {
        Object.assign(account, {
          [`${service}Id`]: accountFromSSO.id,
          isActivated: true
        });
        await account.save();
      } else {
        do {
          randomUserName = generateRandomUsername(accountFromSSO.fullName);
        } while ((await this.accountService.count({ username: randomUserName })) > 0);

        account = await this.accountService.create({
          email: accountFromSSO.email,
          username: randomUserName,
          displayName: accountFromSSO.fullName,
          avatar: accountFromSSO.avatar
        });

        Object.assign(account, {
          [`${service}Id`]: accountFromSSO.id,
          isActivated: true,
          avatar: accountFromSSO.avatar

        });
        await account.save();
      }
    }
    return account;
  }

  async verifyTokenFromEmail(verifyTokenFromEmailDto: VerifyTokenFromEmailDto, type: TokenEnum.EMAIL_VERIFY | TokenEnum.RESET_PASS): Promise<{
    verifiedToken: TokenDocument,
    payload: any
  }> {
    const { verifyToken } = verifyTokenFromEmailDto;
    const payload = this.tokenService.verifyToken(verifyToken, TokenEnum.REFRESH);
    if (payload.type !== type) throw new HttpException("This token is invalid or being revoked", HttpStatus.UNAUTHORIZED);

    const verifiedToken = await this.tokenService.findOne({ token: verifyToken, account: payload.accountId });
    if (!verifiedToken) throw new HttpException("This token is invalid or being revoked", HttpStatus.UNAUTHORIZED);

    return { verifiedToken, payload };
  }
}
