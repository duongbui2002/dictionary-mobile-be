import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { BaseService } from '@common/services/base.service';
import { UserExamDocument } from '@modules/user-exams/user-exam.schema';
import { SubmitQuestionDto } from '@modules/user-exams/dto/submit-question.dto';
import { QuestionsService } from '@modules/questions/questions.service';
import { QuestionDetailsService } from '@modules/question-details/question-details.service';
import { Account } from '@modules/accounts/account.schema';
import { QuestionDetail } from '@modules/question-details/question-detail.schema';
import { ExamsService } from '@modules/exams/exams.service';

@Injectable()
export class UserExamsService extends BaseService<UserExamDocument> {
  constructor(
    @InjectModel('UserExam')
    private readonly userExamModel: PaginateModel<UserExamDocument>,
    private readonly questionDetailService: QuestionDetailsService,
    private readonly examService: ExamsService,
  ) {
    super(userExamModel);
  }

  async userSubmitExam(dto: SubmitQuestionDto, account: Account): Promise<any> {
    let count = 0;
    let tmp: QuestionDetail;
    dto.answersId.map(async (item) => {
      const check = await this.questionDetailService.checkAnswer(item);
      // tmp = check;
      if (check === true) {
        count++;
      }
    });
    const exam = await this.examService.getExamByid(dto.examId);
    // const

    const res = count + '/' + exam.ques.length;
    const newUserExam = new this.userExamModel({
      startTime: dto.startTime,
      endTime: dto.endTime,
      exam: exam,
      account: account,
      result: res,
      status: 1,
    });
    return await newUserExam.save();
  }
}
