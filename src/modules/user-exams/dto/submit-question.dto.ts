import {IsArray, IsDate, IsNotEmpty, IsOptional, IsString} from 'class-validator';

export class SubmitQuestionDto {
  @IsString()
  examId: string;
  @IsDate()
  @IsOptional()
  startTime?: Date;
  @IsDate()
  @IsOptional()
  endTime?: Date;

  @IsArray()
  @IsNotEmpty()
  answersId: string[];
}
