import { Module } from '@nestjs/common';
import { UserExamsService } from './user-exams.service';
import { UserExamsController } from './user-exams.controller';
import { QuestionsModule } from '@modules/questions/questions.module';
import { QuestionDetailsModule } from '@modules/question-details/question-details.module';
import { ExamsModule } from '@modules/exams/exams.module';


@Module({
  providers: [UserExamsService],
  controllers: [UserExamsController],
  imports: [ExamsModule, QuestionDetailsModule],
})
export class UserExamsModule {}
