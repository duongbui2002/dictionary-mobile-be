import { Body, Controller, HttpCode, Post, UseGuards } from '@nestjs/common';
import { ExamsService } from '@modules/exams/exams.service';
import { UserExamsService } from '@modules/user-exams/user-exams.service';
import { AuthGuard } from '@common/guards/auth.guard';
import { CreateExamDto } from '@modules/exams/dto/create-exam.dto';
import { AccountDecorator } from '@common/decorators/account.decorator';
import { Account } from '@modules/accounts/account.schema';
import { SubmitQuestionDto } from '@modules/user-exams/dto/submit-question.dto';

@Controller('user-exams')
export class UserExamsController {
  constructor(private userExamService: UserExamsService) {}

  @Post()
  @HttpCode(201)
  @UseGuards(AuthGuard)
  async createOrder(
    @Body() dto: SubmitQuestionDto,
    @AccountDecorator() account: Account,
  ): Promise<any> {
    return this.userExamService.userSubmitExam(dto, account);
  }
}
