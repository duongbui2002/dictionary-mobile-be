import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as paginate from 'mongoose-paginate-v2';
import mongoose from 'mongoose';
import { ExamDocument } from '@modules/exams/exam.schema';
import { AccountDocument } from '@modules/accounts/account.schema';

export type UserExamDocument = UserExam & Document;

@Schema({
  timestamps: true,
})
export class UserExam {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Exam',
  })
  exam: ExamDocument;
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Account',
  })
  account: AccountDocument;
  @Prop({
    type: Date,
  })
  startTime: Date;
  @Prop({
    type: Date,
  })
  endTime: Date;
  @Prop({
    type: String,
  })
  result: string;

  @Prop({
    type: Number,
  })
  status: number;
}

export const UserExamSchema = SchemaFactory.createForClass(UserExam);

UserExamSchema.plugin(paginate);
// UserExamSchema.virtual('questionDetails', {
//     ref: 'QuestionDetail',
//     localField: '_id',
//     foreignField: 'question',
// });
