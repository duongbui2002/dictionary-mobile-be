FROM node:16-alpine as builder

ENV NODE_ENV build

USER node
WORKDIR /home/node

COPY package*.json ./

RUN yarn install --frozen-lockfile

COPY --chown=node:node . .

RUN yarn build

# ---

FROM node:16-alpine as production

ENV NODE_ENV production
ENV MONGO_URI "mongodb://mongo:27017/project_name"

USER node
#WORKDIR /home/node

COPY --from=builder --chown=node:node /home/node/package*.json ./
COPY --from=builder --chown=node:node /home/node/node_modules/ ./node_modules/
COPY --from=builder --chown=node:node /home/node/dist/ ./dist/

CMD ["node", "dist/main.js"]
